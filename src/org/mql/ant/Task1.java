package org.mql.ant;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

public class Task1 extends Task{
    private String name;

    public void execute() throws BuildException{
        System.out.println("Hello " + name);
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name=name;
    }
}